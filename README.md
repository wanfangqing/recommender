# Recommender

#### 介绍
以太坊智能合约实现登录注册功能，适合初学理解sodility面向对象编程

#### 软件架构
软件架构说明

1. 开发框架使用Truffle
1. 开发工具visual studio
1. node v17.0.1
1. npm v8.1.1
1. ruffle v5.4.16



#### 安装教程

1.  在chrome浏览器上安装MetaMask钱包插件，创建钱包，记住助记词 (在合约中需要使用）
2.  MetaMask使用
    2.1、测试网络选择Ropsten
    2.2、因为部署合约需要消耗以太币，可以去对象地址领取（https://faucet.ropsten.be/），输入自己的钱包地址即可
3.  

#### 使用说明

1.  Truffle开发环境搭建
2.  选择开发工具visual studio， 安装Solidity编程语言插件
![输入图片说明](https://images.gitee.com/uploads/images/2021/1105/100601_1fbdb1d7_1308160.png "屏幕截图.png")
3.  安装node后配置环境变量
4. npm install -g npm@8.1.1 
5. npm install -g truffle
6. 编译项目 truffle compile
7. 部署，部署需要消耗以太币  truffle migrate --network ropsten
8. ![输入图片说明](https://images.gitee.com/uploads/images/2021/1105/101100_41cae6c6_1308160.png "屏幕截图.png")
9. 此项目中部署方式选择需要在infura注册账号
https://infura.io/
![输入图片说明](https://images.gitee.com/uploads/images/2021/1105/101247_d9a26d50_1308160.png "屏幕截图.png")
10. 测试智能合约方法
![输入图片说明](https://images.gitee.com/uploads/images/2021/1105/101658_ab0ca2ef_1308160.png "屏幕截图.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
