// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

/** 用户对象信息 */
contract Recommender {
    // 定义用户结构体
    struct User {
        // 合约拥有者
        address _owner; // 个人合约地址
        uint256 level; //会员等级
        uint256 number; // 直推人数
        uint256 results; // 直推业绩
        uint256 algebra; // 矩阵代数
        uint256 amount; // 钱
        string _password; //验证密码
        // 直属下级  key: 当前用户合约地址 => value: 直属下级mapping
        // 直属下级mapping  key：下级用户合约地址： 下级用户信息
        mapping(address => mapping(address => Junior)) juniors;
        mapping(address => Superior) superior; // 直属上级 key： 当前用户合约地址 => value: 直属上级
    }
   
    // 定义直属下级结构体
    struct Junior {
        address addr;
    }

    // 定义直属上级结构体
    struct Superior {
        address addr;
    }

    mapping(address => User) private users; // 账户个人信息

    // 判断用户地址是否存在
    function isExitUserAddress(address _userAddress, string memory _password) public view returns(bool isIndeed) {
        User storage info = users[_userAddress];
        return (info._owner == _userAddress && keccak256(abi.encodePacked(info._password)) == keccak256(abi.encodePacked(_password)));
    }

    // 根据用户地址查询用户信息
    function findUserInfoByAddress(address _userAddress) public view  returns (address, uint, uint, uint, uint, uint) {
        User storage info = users[_userAddress];
        return (info._owner, info.level, info.number, info.results, info.algebra, info.amount);
    }

    // 创建用户信息
    function createUser(address _userAddress, string memory _password, address superior) public {
        // 校验地址是否已经存在
        // require(!isExitUserAddress(_userAddress, _password), "已经被注册!!!");
        require(
            !isExitUserAddress(_userAddress, _password),
            "Is registered"
        );

        User storage user = users[_userAddress];
        user._owner = _userAddress;
        user.level = 1;
        user.number = 0;
        user.results = 0;
        user.algebra = 1;
        user._password = _password;
        user.superior[_userAddress] = Superior(superior);
    }
}
